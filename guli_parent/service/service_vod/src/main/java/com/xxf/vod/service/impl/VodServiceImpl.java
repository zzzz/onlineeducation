package com.xxf.vod.service.impl;

import com.aliyun.vod.upload.impl.UploadVideoImpl;
import com.aliyun.vod.upload.req.UploadStreamRequest;
import com.aliyun.vod.upload.resp.UploadStreamResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.utils.StringUtils;
import com.aliyuncs.vod.model.v20170321.DeleteVideoRequest;
import com.aliyuncs.vod.model.v20170321.DeleteVideoResponse;
import com.xxf.commonutils.ResultCode;
import com.xxf.servicebase.exceptionhandler.GuliException;
import com.xxf.vod.service.VodService;
import com.xxf.vod.utils.ConstantVideoUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

@Service
public class VodServiceImpl implements VodService {
    @Override
    public String uploadAlyVideo(MultipartFile file) {
        String id=null;
        try{
            String accessKeyId= ConstantVideoUtils.KEY_ID;
            String accessKeySecret=ConstantVideoUtils.KEY_SECRET;
            String fileName=file.getOriginalFilename();
            String title=fileName.substring(0,fileName.lastIndexOf("."));
            InputStream inputStream=file.getInputStream();
            UploadStreamRequest request = new UploadStreamRequest(accessKeyId, accessKeySecret, title, fileName, inputStream);
            UploadVideoImpl uploader = new UploadVideoImpl();
            UploadStreamResponse response = uploader.uploadStream(request);
            id=response.getVideoId();

        }catch (Exception e){
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public boolean deleteAlyVideo(String id) {
        String accessKeyId= ConstantVideoUtils.KEY_ID;
        String accessKeySecret=ConstantVideoUtils.KEY_SECRET;
        DeleteVideoResponse response=null;
        try {
        DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);
        response=deleteVideo(client,id);
        } catch (Exception e) {
            System.out.print("ErrorMessage = " + e.getLocalizedMessage());
            throw new GuliException(ResultCode.ERROR,"删除视频失败");
        }
        return !StringUtils.isEmpty(response.getRequestId());

    }

    @Override
    public void deleteAlyVideoBatch(List<String> videoIds) {
        String accessKeyId= ConstantVideoUtils.KEY_ID;
        String accessKeySecret=ConstantVideoUtils.KEY_SECRET;
        DeleteVideoResponse response=null;
        String ids=org.apache.commons.lang.StringUtils.join(videoIds,",");
        try {
            DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);
            response=deleteVideo(client,ids);
        } catch (Exception e) {
            System.out.print("ErrorMessage = " + e.getLocalizedMessage());
            throw new GuliException(ResultCode.ERROR,"删除视频失败");
        }
    }

    public static DeleteVideoResponse deleteVideo(DefaultAcsClient client,String VideoId) throws Exception {
        DeleteVideoRequest request = new DeleteVideoRequest();
        //支持传入多个视频ID，多个用逗号分隔
        request.setVideoIds(VideoId);
        return client.getAcsResponse(request);
    }

    public  DefaultAcsClient initVodClient(String accessKeyId, String accessKeySecret) throws ClientException {
        String regionId = "cn-shanghai";  // 点播服务接入区域
        DefaultProfile profile = DefaultProfile.getProfile(regionId, accessKeyId, accessKeySecret);
        DefaultAcsClient client = new DefaultAcsClient(profile);
        return client;
    }
}
