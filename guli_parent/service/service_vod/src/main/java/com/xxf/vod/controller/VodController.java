package com.xxf.vod.controller;

import com.xxf.commonutils.R;
import com.xxf.vod.service.VodService;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.HeaderParam;
import java.util.List;


@RestController
@RequestMapping("/eduvod/video")
@CrossOrigin
public class VodController {

    @Autowired
    private VodService vodService;

    @PostMapping("/uploadAlyVideo")
    public R uploadAlyVideo(MultipartFile file){
        String id=vodService.uploadAlyVideo(file);
        return R.ok().data("id",id);
    }

    @DeleteMapping("/deleteAlyVideo/{id}")
    public R deleteAlyVideo(@PathVariable String id){
        boolean delete = vodService.deleteAlyVideo(id);
        return R.ok().data("delete",delete);
    }

    @DeleteMapping("/deleteAlyVideoBatch")
    public R deleteAlyVideoBatch(@RequestParam("videoIds") List<String> videoIds){
        vodService.deleteAlyVideoBatch(videoIds);
        return R.ok();
    }
}
