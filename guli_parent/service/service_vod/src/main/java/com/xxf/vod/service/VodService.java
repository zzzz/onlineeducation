package com.xxf.vod.service;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface VodService {
    String uploadAlyVideo(MultipartFile file);

    boolean deleteAlyVideo(String id);

    void deleteAlyVideoBatch(@RequestParam("videoIds") List<String> videoIds);
}
