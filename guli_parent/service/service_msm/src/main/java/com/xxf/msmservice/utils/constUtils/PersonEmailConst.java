package com.xxf.msmservice.utils.constUtils;


import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PersonEmailConst  implements InitializingBean {
    @Value("${spring.mail.emailFrom}")
    public  String emailFrom;

    public static String EMAIL_FROM;

    @Override
    public void afterPropertiesSet() throws Exception {
        EMAIL_FROM=emailFrom;
    }
}
