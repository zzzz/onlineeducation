package com.xxf.msmservice.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.xxf.msmservice.service.MsmService;
import com.xxf.msmservice.utils.RandomUtil;
import com.xxf.msmservice.utils.constUtils.PersonEmailConst;
import com.xxf.servicebase.exceptionhandler.GuliException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class MsmServiceImpl implements MsmService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private JavaMailSenderImpl sender;

    @Override
    public boolean send(Map<String, String> param,String phone) {
        if(StringUtils.isEmpty(phone)) return false;

        String regionId="default";
        String keyid="LTAI4GAFxHhRuqmZQxeedbRM";
        String keysecret="sWtcac9LoUhc58nhgAwsbZAqufXYDB";
        DefaultProfile profile = DefaultProfile.getProfile(regionId, keyid, keysecret);
        IAcsClient client=new DefaultAcsClient(profile);

        //设置相关固定的参数
        CommonRequest request = new CommonRequest();
        //request.setProtocol(ProtocolType.HTTPS);
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");

        //设置发送相关的参数
        request.putQueryParameter("PhoneNumbers",phone); //手机号
        request.putQueryParameter("SignName","我的谷粒在线教育网站"); //申请阿里云 签名名称
        request.putQueryParameter("TemplateCode","SMS_180051135"); //申请阿里云 模板code
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(param)); //验证码数据，转换json数据传递

        try {
            //最终发送
            CommonResponse response = client.getCommonResponse(request);
            boolean success = response.getHttpResponse().isSuccess();
            return success;
        }catch(Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean sendEamil(String email) {
        if(StringUtils.isEmpty(email))
            return false;
        String code= RandomUtil.getFourBitRandom();
        MimeMessage message = sender.createMimeMessage();
        try{
            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setSubject("注册账号");
            helper.setTo(email);
            helper.setFrom(PersonEmailConst.EMAIL_FROM);
            helper.setText("您的验证码为："+code+",有效时间为5分钟！");
            sender.send(message);
            //以email为key,code为value,有效时间5分钟
            redisTemplate.opsForValue().set(email,code,5, TimeUnit.MINUTES);
            return true;
        }catch (Exception e){
            throw new GuliException(20001,"发送失败");
        }


    }
}
