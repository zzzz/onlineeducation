package com.xxf.msmservice.controller;

import com.xxf.commonutils.R;
import com.xxf.msmservice.service.MsmService;
import com.xxf.msmservice.utils.RandomUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/edumsm/msm")
@CrossOrigin
public class MsmController {

    @Autowired
    private MsmService msmService;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @GetMapping("send/{phone}")
    public R send(@PathVariable String phone){
        String code=redisTemplate.opsForValue().get(phone);
        if(!StringUtils.isEmpty(code))
            return R.ok();

        code= RandomUtil.getFourBitRandom();
        Map<String,String> param=new HashMap<>();
        param.put("code",code);
        boolean isSend=msmService.send(param,phone);
        if(isSend){
            redisTemplate.opsForValue().set(phone,code,5, TimeUnit.MINUTES);
            return R.ok();
        }
        return R.error().message("发送短信失败");
    }

    @GetMapping("/sendEamil/{email}")
    public R sendEamil(@PathVariable String email){
        boolean isSend=msmService.sendEamil(email);
        if(isSend)
            return R.ok();
        return R.error().message("发送失败");
    }
}
