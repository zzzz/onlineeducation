package com.xxf.msmservice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SendEmailTest {


    @Autowired
    private JavaMailSenderImpl sender;
    @Test
    public void send() throws MessagingException {

        MimeMessage message = sender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message);
        helper.setSubject("小标题");
        helper.setTo("844183251@qq.com");
        helper.setFrom("1837719359@qq.com");
        helper.setText("Thank you for ordering!");

        sender.send(message);
    }
}
