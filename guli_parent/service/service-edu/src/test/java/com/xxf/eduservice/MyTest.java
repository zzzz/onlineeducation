package com.xxf.eduservice;

import com.xxf.eduservice.mapper.EduChapterMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyTest {



    public static void main(String[] args) {
        Calendar cal=Calendar.getInstance();
        int year=cal.get(Calendar.YEAR);
        int day=cal.get(Calendar.DAY_OF_YEAR);
        String date=getDate(cal.get(Calendar.DAY_OF_WEEK));
        System.out.println("今天是"+year+"年的第"+day+"天,"+date);
    }

    public static String getDate(int date){
        String res="";
        switch (date){
            case 1:
                res="星期天";
                break;
            case 2:
                res="星期一";
                break;
            case 3:
                res="星期二";
                break;
            case 4:
                res="星期三";
                break;
            case 5:
                res="星期四";
                break;
            case 6:
                res="星期五";
                break;
            case 7:
                res="星期六";
                break;
        }
        return res;
    }

    @Test
    public void two(){
        String regex="([0-9]*)([a-z]*)";
        String input="*****1837719359123zzzz****";
        Pattern pattern=Pattern.compile(regex);
        Matcher matcher=pattern.matcher(input);
        System.out.println(matcher.groupCount());
        while(matcher.find()){
            System.out.println(matcher.group()+":"+matcher.groupCount());
        }
    }

}
