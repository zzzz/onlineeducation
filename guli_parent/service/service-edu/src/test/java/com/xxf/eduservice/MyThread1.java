package com.xxf.eduservice;

public class MyThread1 extends Thread{
    private String name ;
    public MyThread1(String name){
            this.name = name ;      // 通过构造方法配置name属性
    }

    public void run(){  // 覆写run()方法，作为线程 的操作主体
            for(int i=0;i<10;i++){
            System.out.println(name + "运行，i = " + i) ;
            }
    }

}