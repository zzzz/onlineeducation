package com.xxf.eduservice.excel;

import com.alibaba.excel.EasyExcel;
import com.xxf.eduservice.entity.EduSubject;
import com.xxf.eduservice.entity.excel.SubjectData;
import com.xxf.eduservice.service.EduSubjectService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestEasyExcel {
    @Autowired
    EduSubjectService eduSubjectService;

    @Test
    public void writeExcel(){

        List<EduSubject> list = eduSubjectService.list(null);
        List<SubjectData> res=new ArrayList<>();
        for (EduSubject subject : list) {
            if(!subject.getParentId().equals("0")){
                String pid=subject.getParentId();
                String title=subject.getTitle();
                EduSubject pes=eduSubjectService.getById(pid);
                String pTitle=pes.getTitle();

                SubjectData subjectData=new SubjectData();
                subjectData.setOneSubjectName(pTitle);
                subjectData.setTwoSubjectName(title);
                res.add(subjectData);
            }
        }
        String filename="d:\\write.xlsx";

        EasyExcel.write(filename,SubjectData.class).sheet("分类课程列表").doWrite(res);


    }

    @Test
    public void readExcel(){
       /* String filename="d:\\write.xlsx";
        EasyExcel.read(filename,DemoData.class,new ExcelListener()).sheet().doRead();*/
    }

    public List<DemoData> getList(){
        List<DemoData> list=new ArrayList<>();
        for(int i=0;i<10;i++){
            DemoData data=new DemoData();
            data.setSno(i);
            data.setSname("lucy"+i);
            list.add(data);
        }
        return list;
    }
}
