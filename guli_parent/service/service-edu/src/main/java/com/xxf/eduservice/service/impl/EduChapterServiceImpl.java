package com.xxf.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xxf.commonutils.ResultCode;
import com.xxf.eduservice.entity.EduChapter;
import com.xxf.eduservice.entity.EduVideo;
import com.xxf.eduservice.entity.vo.chapter.ChapterVo;
import com.xxf.eduservice.entity.vo.chapter.VideoVo;
import com.xxf.eduservice.mapper.EduChapterMapper;
import com.xxf.eduservice.service.EduChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxf.eduservice.service.EduVideoService;
import com.xxf.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-03-09
 */
@Service
public class EduChapterServiceImpl extends ServiceImpl<EduChapterMapper, EduChapter> implements EduChapterService {

    @Autowired
    private EduVideoService eduVideoService;

    @Override
    public List<ChapterVo> getChapterList(String courseId) {
        //所有章节
        QueryWrapper<EduChapter> chapterQueryWrapper=new QueryWrapper();
        chapterQueryWrapper.eq("course_id",courseId);
        List<EduChapter> eduChapterList=this.list(chapterQueryWrapper);

        //所有小节
        QueryWrapper<EduVideo> videoQueryWrapper=new QueryWrapper();
        videoQueryWrapper.eq("course_id",courseId);
        List<EduVideo> eduVideoList=eduVideoService.list(videoQueryWrapper);

        //章节Vo
        Map<String,ChapterVo> chapterVoMap=new HashMap();
        for(int i=0;i<eduChapterList.size();i++){
            EduChapter eduChapter=eduChapterList.get(i);
            ChapterVo chapterVo= new ChapterVo();
            BeanUtils.copyProperties(eduChapter,chapterVo);
            chapterVoMap.put(eduChapter.getId(),chapterVo);
        }


        for(int i=0;i<eduVideoList.size();i++){

            EduVideo eduVideo=eduVideoList.get(i);
            VideoVo videoVo=new VideoVo();
            BeanUtils.copyProperties(eduVideo,videoVo);
            //根据小结的chapterId查找里面的章节，然后添加进章节里面
            ChapterVo chapterVo = chapterVoMap.get(eduVideo.getChapterId());
            chapterVo.getVideoVoList().add(videoVo);
        }

        //将章节Map里的values转换成List
        List<ChapterVo> chapterVoList=new ArrayList<>(chapterVoMap.values());

        return chapterVoList;
    }

    @Override
    public boolean deleteChapter(String id) {

        QueryWrapper<EduVideo> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("chapter_id",id);
        int count = eduVideoService.count(queryWrapper);

        if(count>0)
            throw new GuliException(ResultCode.ERROR,"不可删除该章节!");

        boolean b = this.removeById(id);

        return b;
    }
}
