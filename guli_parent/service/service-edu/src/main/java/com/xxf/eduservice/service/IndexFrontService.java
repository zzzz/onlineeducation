package com.xxf.eduservice.service;

import com.xxf.commonutils.R;

public interface IndexFrontService {
    R index();
}
