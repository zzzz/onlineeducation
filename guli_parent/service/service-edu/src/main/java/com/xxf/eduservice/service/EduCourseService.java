package com.xxf.eduservice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxf.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xxf.eduservice.entity.vo.CourseInfoVo;
import com.xxf.eduservice.entity.vo.CoursePublishVo;
import com.xxf.eduservice.entity.vo.CourseQuery;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-03-09
 */
public interface EduCourseService extends IService<EduCourse> {

    String saveCourseInfo(CourseInfoVo courseInfoVo);

    CourseInfoVo getCourseInfo(String courseId);

    boolean updateCourseInfo(CourseInfoVo courseInfoVo);

    CoursePublishVo getPublishCourseInfo(String courseId);

    Page<EduCourse> getConditionPageCourseList(long current, long limit, CourseQuery courseQuery);

    boolean deleteCourse(String id);
}
