package com.xxf.eduservice.mapper;

import com.xxf.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxf.eduservice.entity.vo.CoursePublishVo;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2021-03-09
 */
public interface EduCourseMapper extends BaseMapper<EduCourse> {
     CoursePublishVo getPublishCourseInfo(String courseId);
}
