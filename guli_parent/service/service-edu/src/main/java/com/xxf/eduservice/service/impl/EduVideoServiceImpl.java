package com.xxf.eduservice.service.impl;

import com.xxf.commonutils.R;
import com.xxf.commonutils.ResultCode;
import com.xxf.eduservice.client.VodClient;
import com.xxf.eduservice.entity.EduVideo;
import com.xxf.eduservice.mapper.EduVideoMapper;
import com.xxf.eduservice.service.EduVideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxf.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-03-09
 */
@Service
public class EduVideoServiceImpl extends ServiceImpl<EduVideoMapper, EduVideo> implements EduVideoService {

    @Autowired
    private VodClient vodClient;

    @Transactional
    @Override
    public boolean deleteVideo(String id) {
        EduVideo eduVideo = this.getById(id);
        String videoSourceId = eduVideo.getVideoSourceId();
        //删除阿里云视频
        R r = vodClient.deleteAlyVideo(videoSourceId);
        boolean delete1= (boolean)r.getData().get("delete");
        //删除数据库小节
        boolean delete2 = this.removeById(id);

        return delete1 && delete2;
    }
}
