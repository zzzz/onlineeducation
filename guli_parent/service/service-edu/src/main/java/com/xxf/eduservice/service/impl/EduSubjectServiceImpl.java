package com.xxf.eduservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.xxf.eduservice.entity.EduSubject;
import com.xxf.eduservice.entity.excel.SubjectData;
import com.xxf.eduservice.entity.subject.SubjectTree;
import com.xxf.eduservice.listener.SujectExcelListener;
import com.xxf.eduservice.mapper.EduSubjectMapper;
import com.xxf.eduservice.service.EduSubjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-03-08
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    @Override
    public void savaSubject(MultipartFile file,EduSubjectService eduSubjectService) {
        try {
            EasyExcel.read(file.getInputStream(), SubjectData.class,new SujectExcelListener(eduSubjectService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<SubjectTree> getSubjectList() {
        //查询按默认循序查，默认循序就是一级目录永远在二级目录之前插入
        List<EduSubject> list = this.list(null);
        //下面逻辑就是，查询到一级目录就加入集合中，然后二级再加入一级中；如果不按默认查询，就可能出现null
        Map<String,SubjectTree> treeMap=new HashMap<>();

        List<SubjectTree> subjectTrees=new ArrayList<>();

        for (EduSubject subject : list) {
            //说明是一级目录
            if(subject.getParentId().equals("0")){
                SubjectTree tree=new SubjectTree();
                tree.setId(subject.getId());
                tree.setLabel(subject.getTitle());

                BeanUtils.copyProperties(subject,tree);
                //添加进一级目录集合里
                subjectTrees.add(tree);
                //记录一级目录位置,便于二级目录搜索
                treeMap.put(subject.getId(),tree);
            }
            //二级目录
            else{
                String pid=subject.getParentId();
                //子树
                SubjectTree children=new SubjectTree();
                children.setId(subject.getId());
                children.setLabel(subject.getTitle());
                try{
                    //添加进父树
                    SubjectTree parent_tree=treeMap.get(pid);
                    parent_tree.getChildren().add(children);
                }catch(Exception e){

                }

            }
        }
        return subjectTrees;
    }


}
