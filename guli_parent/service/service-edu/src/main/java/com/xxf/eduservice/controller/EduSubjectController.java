package com.xxf.eduservice.controller;


import com.xxf.commonutils.R;
import com.xxf.eduservice.entity.EduSubject;
import com.xxf.eduservice.entity.subject.SubjectTree;
import com.xxf.eduservice.service.EduSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-03-08
 */
@RestController
@RequestMapping("/eduservice/subject")
@CrossOrigin
public class EduSubjectController {

    @Autowired
    private EduSubjectService eduSubjectService;

    @PostMapping("/addSubject")
    public R addSubject(MultipartFile file){
        eduSubjectService.savaSubject(file,eduSubjectService);
        return R.ok();
    }

    @PostMapping("/getSubjectList")
    public R getSubjectList(){

        List<SubjectTree> trees=eduSubjectService.getSubjectList();

        return R.ok().data("data",trees);
    }

}

