package com.xxf.eduservice.controller.front;

import com.xxf.commonutils.R;
import com.xxf.eduservice.service.EduCourseService;
import com.xxf.eduservice.service.EduTeacherService;
import com.xxf.eduservice.service.IndexFrontService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/eduservice/indexfront")
@CrossOrigin
public class IndexFrontController {


    @Autowired
    private IndexFrontService indexFrontService;

    @GetMapping("/index")
    public R index(){
        return indexFrontService.index();
    }
}
