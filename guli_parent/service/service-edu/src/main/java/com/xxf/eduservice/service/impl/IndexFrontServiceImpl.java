package com.xxf.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xxf.commonutils.R;
import com.xxf.eduservice.entity.EduCourse;
import com.xxf.eduservice.entity.EduTeacher;
import com.xxf.eduservice.service.EduCourseService;
import com.xxf.eduservice.service.EduTeacherService;
import com.xxf.eduservice.service.IndexFrontService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndexFrontServiceImpl implements IndexFrontService {

    @Autowired
    private EduCourseService eduCourseService;

    @Autowired
    private EduTeacherService eduTeacherService;

    @Cacheable(key="'index'",value="course_teacher")
    //前八条热门课程，前四位热门讲师
    @Override
    public R index() {
        QueryWrapper<EduCourse> courseQueryWrapper=new QueryWrapper<>();
        courseQueryWrapper.orderByDesc("id");
        //拼接语句
        courseQueryWrapper.last("limit 8");
        List<EduCourse> courseList = eduCourseService.list(courseQueryWrapper);

        QueryWrapper<EduTeacher> teacherQueryWrapper=new QueryWrapper<>();
        teacherQueryWrapper.orderByDesc("id");
        //拼接语句
        teacherQueryWrapper.last("limit 4");
        List<EduTeacher> teacherList = eduTeacherService.list(teacherQueryWrapper);

        return R.ok().data("courseList",courseList).data("teacherList",teacherList);
    }
}
