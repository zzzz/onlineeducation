package com.xxf.eduservice.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class SubjectData {

    /*
    * 数据模板
    *     列1     列2
    *     前端     js
    *     前端     html
    *     后端     php
    *     后端     java
    * */

    @ExcelProperty(value="一级目录",index = 0)
    private String oneSubjectName;

    @ExcelProperty(value="二级目录",index=1)
    private String twoSubjectName;
}
