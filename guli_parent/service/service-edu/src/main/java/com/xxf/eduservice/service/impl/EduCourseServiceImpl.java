package com.xxf.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.databind.util.BeanUtil;
import com.xxf.commonutils.ResultCode;
import com.xxf.eduservice.client.VodClient;
import com.xxf.eduservice.entity.EduChapter;
import com.xxf.eduservice.entity.EduCourse;
import com.xxf.eduservice.entity.EduCourseDescription;
import com.xxf.eduservice.entity.EduVideo;
import com.xxf.eduservice.entity.vo.CourseInfoVo;
import com.xxf.eduservice.entity.vo.CoursePublishVo;
import com.xxf.eduservice.entity.vo.CourseQuery;
import com.xxf.eduservice.mapper.EduCourseMapper;
import com.xxf.eduservice.service.EduChapterService;
import com.xxf.eduservice.service.EduCourseDescriptionService;
import com.xxf.eduservice.service.EduCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxf.eduservice.service.EduVideoService;
import com.xxf.servicebase.exceptionhandler.GuliException;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-03-09
 */
@Service
public class EduCourseServiceImpl extends ServiceImpl<EduCourseMapper, EduCourse> implements EduCourseService {

    @Autowired
    private EduCourseDescriptionService eduCourseDescriptionService;

    @Autowired
    private EduChapterService eduChapterService;

    @Autowired
    private EduVideoService eduVideoService;

    @Autowired
    private VodClient vodClient;

    //添加课程基本信息
    @Override
    public String saveCourseInfo(CourseInfoVo courseInfoVo) {
        //向课程添加信息
        EduCourse eduCourse=new EduCourse();
        BeanUtils.copyProperties(courseInfoVo,eduCourse);
        boolean save = this.save(eduCourse);
        if(!save)
            throw new GuliException(ResultCode.ERROR,"添加课程信息失败");

        String id=eduCourse.getId();

        //向课程简介添加信息
        EduCourseDescription eduCourseDescription=new EduCourseDescription();
        eduCourseDescription.setId(id);
        eduCourseDescription.setDescription(courseInfoVo.getDescription());
        eduCourseDescriptionService.save(eduCourseDescription);
        return id;
    }

    @Override
    public CourseInfoVo getCourseInfo(String courseId) {
        CourseInfoVo courseInfoVo=new CourseInfoVo();

        EduCourse eduCourse = this.getById(courseId);
        BeanUtils.copyProperties(eduCourse,courseInfoVo);

        EduCourseDescription eduCourseDescription = eduCourseDescriptionService.getById(eduCourse.getId());
        courseInfoVo.setDescription(eduCourseDescription.getDescription());

        return courseInfoVo;
    }

    @Transactional
    @Override
    public boolean updateCourseInfo(CourseInfoVo courseInfoVo) {

        EduCourse eduCourse=new EduCourse();
        EduCourseDescription eduCourseDescription=new EduCourseDescription();

        BeanUtils.copyProperties(courseInfoVo,eduCourse);
        BeanUtils.copyProperties(courseInfoVo,eduCourseDescription);

        QueryWrapper<EduCourse> courseQueryWrapper=new QueryWrapper<>();
        courseQueryWrapper.eq("id",eduCourse.getId());
        boolean updateCourse = this.update(eduCourse, courseQueryWrapper);

        QueryWrapper<EduCourseDescription> courseDescriptionQueryWrapper=new QueryWrapper<>();
        courseDescriptionQueryWrapper.eq("id",eduCourseDescription.getId());
        boolean updateCourseDescription = eduCourseDescriptionService.update(eduCourseDescription, courseDescriptionQueryWrapper);


        return updateCourse && updateCourseDescription;
    }

    @Override
    public CoursePublishVo getPublishCourseInfo(String courseId) {
        CoursePublishVo publishCourseInfo = baseMapper.getPublishCourseInfo(courseId);
        return publishCourseInfo;
    }

    @Override
    public Page<EduCourse> getConditionPageCourseList(long current, long limit, CourseQuery courseQuery) {
        Page<EduCourse> page=new Page<>(current,limit);
        QueryWrapper<EduCourse> queryWrapper=new QueryWrapper<>();
        String title=courseQuery.getTitle();
        BigDecimal minPrice = courseQuery.getMinPrice();
        BigDecimal maxPrice = courseQuery.getMaxPrice();
        String status = courseQuery.getStatus();
        String begin = courseQuery.getBegin();
        String end = courseQuery.getEnd();

        if(!StringUtils.isEmpty(title))
            queryWrapper.like("title",title);

        if(!StringUtils.isEmpty(minPrice))
            queryWrapper.ge("price",minPrice);

        if(!StringUtils.isEmpty(maxPrice))
            queryWrapper.le("price",maxPrice);

        if(!StringUtils.isEmpty(status))
            queryWrapper.eq("status",status);

        if(!StringUtils.isEmpty(begin))
            queryWrapper.ge("gmt_create",begin);

        if(!StringUtils.isEmpty(end))
            queryWrapper.ge("gmt_modified",end);

        this.page(page, queryWrapper);
        return page;
    }

    @Transactional
    @Override
    public boolean deleteCourse(String id) {
        //获取course_id下的所有小节
        QueryWrapper<EduVideo> videoQueryWrapper=new QueryWrapper<>();
        videoQueryWrapper.eq("course_id",id);
        videoQueryWrapper.select("video_source_id");
        List<EduVideo> videoList = eduVideoService.list(videoQueryWrapper);

        //删除所有小节视频
        List<String> videoIds=videoList.stream()
                .map(eduVideo -> eduVideo.getVideoSourceId())
                .filter(i->!i.trim().equals(""))
                .collect(Collectors.toList());

        if(videoIds.size()>0)
            vodClient.deleteAlyVideoBatch(videoIds);

        //删除小节
        videoQueryWrapper=new QueryWrapper<>();
        videoQueryWrapper.eq("course_id",id);
        boolean remove = eduVideoService.remove(videoQueryWrapper);

        //删除章节
        QueryWrapper<EduChapter> chapterQueryWrapper=new QueryWrapper<>();
        chapterQueryWrapper.eq("course_id",id);
        boolean remove1 = eduChapterService.remove(chapterQueryWrapper);

        //删除课程
        boolean remove2 = this.removeById(id);


        return remove && remove1 && remove2;
    }
}
