package com.xxf.eduservice.service;

import com.xxf.eduservice.entity.EduSubject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xxf.eduservice.entity.subject.SubjectTree;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-03-08
 */
public interface EduSubjectService extends IService<EduSubject> {

    void savaSubject(MultipartFile file,EduSubjectService eduSubjectService);

    List<SubjectTree> getSubjectList();
}
