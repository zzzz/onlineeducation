package com.xxf.eduservice.controller;


import com.xxf.commonutils.R;
import com.xxf.eduservice.entity.EduChapter;
import com.xxf.eduservice.entity.vo.chapter.ChapterVo;
import com.xxf.eduservice.service.EduChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-03-09
 */
@RestController
@RequestMapping("/eduservice/chapter")
@CrossOrigin
public class EduChapterController {

    @Autowired
    private EduChapterService eduChapterService;

    @GetMapping("/getChapterList/{courseId}")
    public R getChapterList(@PathVariable String courseId){
        List<ChapterVo> chapterVoList= eduChapterService.getChapterList(courseId);
        return R.ok().data("list",chapterVoList);
    }

    @GetMapping("/getChapter/{id}")
    public R getChapter(@PathVariable String id){
        EduChapter eduChapter = eduChapterService.getById(id);
        return R.ok().data("eduChapter",eduChapter);
    }

    @PostMapping("/addChapter")
    public R addChapter(@RequestBody EduChapter eduChapter){
        boolean save = eduChapterService.save(eduChapter);
        return R.ok().data("add",save);
    }

    @DeleteMapping("/deleteChapter/{id}")
    public R deleteChapter(@PathVariable String id){
        boolean delete = eduChapterService.deleteChapter(id);
        return R.ok().data("delete",delete);
    }

    @PostMapping("/updateChapter")
    public R updateChapter(@RequestBody EduChapter eduChapter){
        boolean b = eduChapterService.updateById(eduChapter);
        return R.ok().data("update",b);
    }

}

