package com.xxf.eduservice.mapper;

import com.xxf.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2021-03-09
 */
public interface EduChapterMapper extends BaseMapper<EduChapter> {

}
