package com.xxf.eduservice.service;

import com.xxf.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-03-09
 */
public interface EduVideoService extends IService<EduVideo> {

    boolean deleteVideo(String id);
}
