package com.xxf.eduservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxf.commonutils.R;
import com.xxf.eduservice.entity.EduCourse;
import com.xxf.eduservice.entity.vo.CourseInfoVo;
import com.xxf.eduservice.entity.vo.CoursePublishVo;
import com.xxf.eduservice.entity.vo.CourseQuery;
import com.xxf.eduservice.service.EduCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-03-09
 */
@RestController
@RequestMapping("/eduservice/course")
@CrossOrigin
public class EduCourseController {
    @Autowired
    private EduCourseService courseService;

    @GetMapping("/getPageCourseList/{current}/{limit}")
    public R getPageCourseList(@PathVariable long current,@PathVariable long limit){
        Page<EduCourse> page=new Page<>(current,limit);
        courseService.page(page,null);
        long total=page.getTotal();
        List<EduCourse> records = page.getRecords();
        return R.ok().data("total",total).data("records",records);

    }

    @PostMapping("/getConditionPageCourseList/{current}/{limit}")
    public R getConditionPageCourseList(@PathVariable long current,@PathVariable long limit, @RequestBody(required = false) CourseQuery courseQuery){
        Page<EduCourse> page= courseService.getConditionPageCourseList(current,limit,courseQuery);
        long total=page.getTotal();
        List<EduCourse> records = page.getRecords();

        return R.ok().data("total",total).data("records",records);
    }

    @PostMapping("/addCourseInfo")
    public R addCourseInfo(@RequestBody CourseInfoVo courseInfoVo){
        String id=courseService.saveCourseInfo(courseInfoVo);
        return R.ok().data("id",id);
    }

    @GetMapping("/getCourseInfo/{courseId}")
    public R getCourseInfo(@PathVariable String courseId){
        CourseInfoVo courseInfoVo=courseService.getCourseInfo(courseId);
        return R.ok().data("courseInfoVo",courseInfoVo);
    }

    @PostMapping("/updateCourseInfo")
    public R updateCourseInfo(@RequestBody CourseInfoVo courseInfoVo){

        boolean update=courseService.updateCourseInfo(courseInfoVo);

        return R.ok().data("update",update);
    }

    @GetMapping("/getPublishCourseInfo/{id}")
    public R getPublishCourseInfo(@PathVariable String id){
        CoursePublishVo publishCourseInfo = courseService.getPublishCourseInfo(id);
        return R.ok().data("publishCourseInfo",publishCourseInfo);
    }

    @GetMapping("/publishCourse/{id}")
    public R publishCourse(@PathVariable String id){
        EduCourse eduCourse=new EduCourse();
        eduCourse.setId(id);
        eduCourse.setStatus("Normal");
        boolean publish = courseService.updateById(eduCourse);
        return R.ok().data("publish",publish);
    }

    @DeleteMapping("/deleteCourse/{id}")
    public R deleteCourse(@PathVariable String id){
        boolean delete = courseService.deleteCourse(id);
        return R.ok().data("delete",delete);
    }
}

