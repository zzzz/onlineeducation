package com.xxf.eduservice.client;

import com.xxf.commonutils.R;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

//熔断器
@Component
public class VodClientFeignClient implements  VodClient{
    @Override
    public R deleteAlyVideo(String id) {
        return R.error().message("删除视频出现错误");
    }

    @Override
    public R deleteAlyVideoBatch(List<String> videoIds) {
        return R.error().message("删除多个视频出现错误");
    }
}
