package com.xxf.eduservice.service;

import com.xxf.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-03-09
 */
public interface EduCourseDescriptionService extends IService<EduCourseDescription> {

}
