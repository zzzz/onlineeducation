package com.xxf.eduservice.service.impl;

import com.xxf.eduservice.entity.EduTeacher;
import com.xxf.eduservice.mapper.EduTeacherMapper;
import com.xxf.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 讲师 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-02-07
 */
@Service
public class EduTeacherServiceImpl extends ServiceImpl<EduTeacherMapper, EduTeacher> implements EduTeacherService {

}
