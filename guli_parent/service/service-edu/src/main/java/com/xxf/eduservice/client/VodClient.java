package com.xxf.eduservice.client;

import com.xxf.commonutils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name="service-vod",fallback = VodClientFeignClient.class)
@Component
public interface VodClient {

    @DeleteMapping(value="/eduvod/video/deleteAlyVideo/{id}")
    R deleteAlyVideo(@PathVariable("id") String id);

    @DeleteMapping(value="/eduvod/video/deleteAlyVideoBatch")
    R deleteAlyVideoBatch(@RequestParam("videoIds") List<String> videoIds);
}
