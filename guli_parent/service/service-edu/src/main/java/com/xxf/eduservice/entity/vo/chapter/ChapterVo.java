package com.xxf.eduservice.entity.vo.chapter;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ChapterVo {
    private String id;

    private String title;

    private List<VideoVo> videoVoList=new ArrayList<>();
}
