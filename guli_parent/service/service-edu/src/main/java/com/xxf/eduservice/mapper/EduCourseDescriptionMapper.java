package com.xxf.eduservice.mapper;

import com.xxf.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程简介 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2021-03-09
 */
public interface EduCourseDescriptionMapper extends BaseMapper<EduCourseDescription> {

}
