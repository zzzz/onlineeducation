package com.xxf.eduservice.entity.vo;

import lombok.Data;

import java.math.BigDecimal;


@Data
public class CourseQuery {

    private String title;

    private BigDecimal minPrice;

    private BigDecimal maxPrice;

    private String status;

    private String begin;

    private String end;
}
