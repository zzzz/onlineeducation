package com.xxf.eduservice.controller;


import com.xxf.commonutils.R;
import com.xxf.eduservice.client.VodClient;
import com.xxf.eduservice.entity.EduVideo;
import com.xxf.eduservice.service.EduVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-03-09
 */
@RestController
@RequestMapping("/eduservice/video")
@CrossOrigin
public class EduVideoController {

    @Autowired
    private EduVideoService eduVideoService;


    @GetMapping("/getVideo/{id}")
    public R getVideo(@PathVariable String id){
        EduVideo eduVideo = eduVideoService.getById(id);
        return R.ok().data("eduVideo",eduVideo);
    }

    @PostMapping("/addVideo")
    public R addVideo(@RequestBody EduVideo eduVideo){
        boolean save = eduVideoService.save(eduVideo);
        return R.ok().data("add",save);
    }

    @DeleteMapping("/deleteVideo/{id}")
    public R deleteVideo(@PathVariable String id){
        boolean delete= eduVideoService.deleteVideo(id);
        return R.ok().data("delete",delete);
    }

    @PostMapping("/updateVideo")
    public R updateVideo(@RequestBody EduVideo eduVideo){
        boolean update = eduVideoService.updateById(eduVideo);
        return R.ok().data("update",update);
    }

}

