package com.xxf.eduservice.service;

import com.xxf.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xxf.eduservice.entity.vo.chapter.ChapterVo;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-03-09
 */
public interface EduChapterService extends IService<EduChapter> {

    List<ChapterVo> getChapterList(String courseId);

    boolean deleteChapter(String id);
}
