package com.xxf.educenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sun.java.accessibility.util.GUIInitializedListener;
import com.xxf.commonutils.JwtUtils;
import com.xxf.commonutils.MD5;
import com.xxf.educenter.entity.UcenterMember;
import com.xxf.educenter.entity.vo.RegisterVo;
import com.xxf.educenter.mapper.UcenterMemberMapper;
import com.xxf.educenter.service.UcenterMemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxf.servicebase.exceptionhandler.GuliException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-03-17
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Override
    public String login(UcenterMember ucenterMember) {
        //手机号和密码
        String mobile = ucenterMember.getMobile();
        String password = ucenterMember.getPassword();

        if(StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password))
            throw  new GuliException(20001,"登录失败");

        //判断手机是否正确
        QueryWrapper<UcenterMember> wrapper=new QueryWrapper<>();
        wrapper.eq("mobile",mobile);
        UcenterMember one = this.getOne(wrapper);

        if(one==null)
            throw new GuliException(20001,"登录失败");

        if(!MD5.encrypt(password).equals(one.getPassword()))
            throw new GuliException(20001,"登录失败");

        //判断是否禁用
        if(one.getIsDisabled())
            throw new GuliException(20001,"登录失败");

        //生成token字符串
        String jwtToken = JwtUtils.getJwtToken(one.getId(), one.getNickname());

        return jwtToken ;
    }

    @Override
    public void register(RegisterVo registerVo) {
        //获取验证码
        String code=registerVo.getCode();
        String mobile = registerVo.getMobile();
        String nickname = registerVo.getNickname();
        String password = registerVo.getPassword();
        if(StringUtils.isEmpty(code) || StringUtils.isEmpty(mobile) || StringUtils.isEmpty(nickname)
                || StringUtils.isEmpty(password))
            throw new GuliException(20001,"注册失败");

        //判断验证码
        String redisCode = redisTemplate.opsForValue().get(mobile);
        redisCode=redisCode.substring(1);
        redisCode=redisCode.substring(0,redisCode.length()-1);
        System.out.println(redisCode+":"+code);
        if(redisCode==null || !redisCode.equals(code))
            throw new GuliException(20001,"注册失败");

        //判断手机号是否存在
        QueryWrapper<UcenterMember> wrapper=new QueryWrapper<>();
        wrapper.eq("mobile",mobile);
        int count = this.count(wrapper);
        if(count>0)
            throw new GuliException(20001,"注册失败");

        //加入数据库
        UcenterMember ucenterMember=new UcenterMember();
        ucenterMember.setNickname(nickname);
        ucenterMember.setPassword(MD5.encrypt(password));
        ucenterMember.setMobile(mobile);
        ucenterMember.setIsDisabled(false);
        ucenterMember.setAvatar("http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132");
        this.save(ucenterMember);
    }
}
