package com.xxf.educenter.service;

import com.xxf.educenter.entity.UcenterMember;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xxf.educenter.entity.vo.RegisterVo;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-03-17
 */
public interface UcenterMemberService extends IService<UcenterMember> {

    String login(UcenterMember ucenterMember);

    void register(RegisterVo registerVo);
}
