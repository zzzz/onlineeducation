package com.xxf.educms.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxf.commonutils.R;
import com.xxf.educms.entity.CrmBanner;
import com.xxf.educms.service.CrmBannerService;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-03-15
 */
@RestController
@RequestMapping("/educms/banneradmin")
@CrossOrigin
public class BannerAdminController {

    @Autowired
    private CrmBannerService crmBannerService;

    //分页查询banner
    @GetMapping("/pageBanner/{current}/{limit}")
    public R pageBanner(@PathVariable long current, @PathVariable long limit){
        Page<CrmBanner> page=new Page<>(current,limit);
        crmBannerService.page(page, null);
        return R.ok().data("items",page.getRecords()).data("total",page.getTotal());
    }

    //增加
    @PostMapping("/addBanner")
    public R addBanner(@RequestBody CrmBanner crmBanner){
        boolean save = crmBannerService.save(crmBanner);
        return R.ok().data("save",save);
    }

    //删除
    @GetMapping("/deleteBanner/{id}")
    public R deleteBanner(@PathVariable String id){
        boolean delete = crmBannerService.removeById(id);
        return R.ok().data("delete",delete);
    }

    //修改
    @PostMapping("/updateBanner")
    public R updateBanner(@RequestBody CrmBanner crmBanner){
        boolean update = crmBannerService.updateById(crmBanner);
        return R.ok().data("update",update);
    }
}

