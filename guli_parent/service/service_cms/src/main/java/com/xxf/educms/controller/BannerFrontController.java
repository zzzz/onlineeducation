package com.xxf.educms.controller;

import com.xxf.commonutils.R;
import com.xxf.educms.entity.CrmBanner;
import com.xxf.educms.service.CrmBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/educms/bannerfront")
@CrossOrigin
public class BannerFrontController {

    @Autowired
    private CrmBannerService crmBannerService;

    @GetMapping("/getBannerList")
    public R getBannerList(){
        List<CrmBanner> list=crmBannerService.getBannerList();
        return R.ok().data("list",list);
    }

}
