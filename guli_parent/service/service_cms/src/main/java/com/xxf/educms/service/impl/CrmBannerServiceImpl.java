package com.xxf.educms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xxf.educms.entity.CrmBanner;
import com.xxf.educms.mapper.CrmBannerMapper;
import com.xxf.educms.service.CrmBannerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 首页banner表 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-03-15
 */
@Service
public class CrmBannerServiceImpl extends ServiceImpl<CrmBannerMapper, CrmBanner> implements CrmBannerService {

    @Cacheable(key="'selectIndexBanner'",value = "banner")
    @Override
    public List<CrmBanner> getBannerList() {
        QueryWrapper<CrmBanner> queryWrapper=new QueryWrapper<>();
        queryWrapper.orderByDesc("id");
        //拼接语句
        queryWrapper.last("limit 2");
        List<CrmBanner> list = this.list(queryWrapper);
        return list;
    }
}
