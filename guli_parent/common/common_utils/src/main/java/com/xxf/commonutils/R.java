package com.xxf.commonutils;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.xml.transform.Result;
import java.util.HashMap;
import java.util.Map;

@Data
public class R {
    @ApiModelProperty(value = "是否成功")
    private boolean success;

    @ApiModelProperty(value = "返回码")
    private Integer code;

    @ApiModelProperty(value = "返回消息")
    private String message;

    @ApiModelProperty(value = "返回数据")
    private Map<String,Object> data=new HashMap<>();

    //私有构造器
    private R(){}

    public static R ok(){
        R r=new R();
        //链式编程
        r.success(true).code(ResultCode.SUCCESS).message("成功");
        /*r.setSuccess(true);
        r.setCode(ResultCode.SUCCESS);
        r.setMessage("成功");*/
        return r;
    }

    public static R error(){
        R r=new R();
        //链式编程
        r.success(false).code(ResultCode.ERROR).message("失败");
        /*r.setSuccess(false);
        r.setCode(ResultCode.ERROR);
        r.setMessage("失败");*/
        return r;
    }

    public R success(boolean success){
        this.setSuccess(success);
        return this;
    }

    public R code(Integer code){
        this.setCode(code);
        return this;
    }

    public R message(String message){
        this.setMessage(message);
        return this;
    }

    public R data(Map<String,Object> map){
        this.setData(map);
        return this;
    }

    public R data(String key,Object value){
        this.data.put(key,value);
        return this;
    }
}
